import can_proxy_thread as can
from can_ids import CAN_SPEAK_DIRECT

def speak_direct(str):
    bytes = []
    str = str + chr(10)
    for byte in str:
        bytes.append(ord(byte))
        if len(bytes) == 8:
            can.send('tts', CAN_SPEAK_DIRECT, data=bytes)
            bytes = []
    if len(bytes) > 0:
        can.send('tts', CAN_SPEAK_DIRECT, data=bytes)
