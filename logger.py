import logging
import sys
from gdgt_platform import *

class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    """
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(dummy):
        pass

def init(file, log_level=logging.DEBUG):
    if file == None:
        logging.basicConfig(
            stream=sys.stdout,
            level=log_level,
            format='%(asctime)s %(levelname)s: %(message)s',
            datefmt='%H:%M:%S'
        )
    else:
        logging.basicConfig(
            filename=f'{LOG_PATH}/{file}.log.txt',
            level=log_level,
            format='%(asctime)s %(levelname)s: %(message)s',
            datefmt='%H:%M:%S'
        )

        stdout_logger = logging.getLogger('STDOUT')
        sys.stdout = StreamToLogger(stdout_logger, logging.INFO)

        stderr_logger = logging.getLogger('STDERR')
        sys.stderr = StreamToLogger(stderr_logger, logging.ERROR)

    logging.info('==========')
