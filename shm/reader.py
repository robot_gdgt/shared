import numpy as np
import mmap
import logging

from posix_ipc import Semaphore, SharedMemory, ExistentialError
from ctypes import sizeof, memmove, addressof, create_string_buffer
from shm.structures import MD

from time import sleep

import var_dump as vd
from here import here

md_buf = create_string_buffer(sizeof(MD))

dtypes = ('uint8', 'uint16', 'uint32', 'int8', 'int16', 'int32', 'float16', 'float32', 'float64')

class SharedMemoryFrameReader:
    def __init__(self, name):
        logging.debug(here(self))
        self.shm_buf = None
        self.shm_name = name
        self.md_buf = None
        self.count = -1

        while not self.md_buf:
            try:
                region = SharedMemory(name + '-meta')
                self.md_buf = mmap.mmap(region.fd, sizeof(MD))
                region.close_fd()
                logging.info('Aquired MetaData shared memory.')
            except ExistentialError:
                logging.info('Waiting for MetaData shared memory.')
                sleep(1)

        self.sem = Semaphore(name, 0)

    def get(self):
        logging.debug(here(self))
        md = MD()

        self.sem.acquire()
        md_buf[:] = self.md_buf
        memmove(addressof(md), md_buf, sizeof(md))
        self.sem.release()

        if md.count == self.count:
            return (None, self.count)

        while not self.shm_buf:
            try:
                region = SharedMemory(self.shm_name)
                self.shm_buf = mmap.mmap(region.fd, md.size)
                region.close_fd()
                logging.info('Aquired Data shared memory.')
            except ExistentialError:
                logging.info('Waiting for Data shared memory.')
                sleep(1)

        self.sem.acquire()
        if md.shape_1 == 0:
            f = np.ndarray(shape=(md.shape_0), dtype=dtypes[md.dtype], buffer=self.shm_buf)
        elif md.shape_2 == 0:
            f = np.ndarray(shape=(md.shape_0, md.shape_1), dtype=dtypes[md.dtype], buffer=self.shm_buf)
        else:
            f = np.ndarray(shape=(md.shape_0, md.shape_1, md.shape_2), dtype=dtypes[md.dtype], buffer=self.shm_buf)
        self.sem.release()
        self.count = md.count
        return (f, md.count)

    def release(self):
        logging.debug(here(self))
        self.md_buf.close()
        if self.shm_buf: self.shm_buf.close()
        logging.info('Reader terminated')
