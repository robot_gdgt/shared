import numpy as np
import mmap
import logging
import uuid

from posix_ipc import Semaphore, O_CREX, ExistentialError, O_CREAT, SharedMemory, unlink_shared_memory
from ctypes import sizeof, memmove, addressof, create_string_buffer
from shm.structures import MD

import var_dump as vd
from here import here

md_buf = create_string_buffer(sizeof(MD))

dtypes = ('uint8', 'uint16', 'uint32', 'int8', 'int16', 'int32', 'float16', 'float32', 'float64')

class SharedMemoryFrameWriter:
    def __init__(self, name=None):
        logging.debug(here(self))
        self.shm_buf = None
        self.count = 0

        region = None

        while True:
            try:
                if name is None: name = f'/{uuid.uuid4().hex[0:16]}'
                region = SharedMemory(f'{name}-meta', O_CREAT, size=sizeof(MD))
                break
            except:
                name = f'/{uuid.uuid4().hex}'
        self.shm_name = name
        self.md_buf = mmap.mmap(region.fd, sizeof(MD))
        region.close_fd()

        try:
            self.sem = Semaphore(name, O_CREX)
        except ExistentialError:
            sem = Semaphore(name, O_CREAT)
            sem.unlink()
            self.sem = Semaphore(name, O_CREX)
        self.sem.release()

    def add(self, ndarr: np.ndarray):
        logging.debug(here(self))
        byte_size = ndarr.nbytes
        if not self.shm_buf:
            region = SharedMemory(self.shm_name, O_CREAT, size=byte_size)
            self.shm_buf = mmap.mmap(region.fd, byte_size)
            region.close_fd()

        self.count += 1
        if self.count % 10 == 0: logging.info(f'count: {self.count}')
        logging.debug(f'ndarr.dtype: {ndarr.dtype} = {dtypes.index(ndarr.dtype)}')
        md = MD(
            ndarr.shape[0],
            ndarr.shape[1] if ndarr.ndim > 1 else 0,
            ndarr.shape[2] if ndarr.ndim > 2 else 0,
            dtypes.index(ndarr.dtype),
            byte_size,
            self.count
        )
        self.sem.acquire()
        memmove(md_buf, addressof(md), sizeof(md))
        self.md_buf[:] = bytes(md_buf)
        self.shm_buf[:] = ndarr.tobytes()
        self.sem.release()

    def release(self):
        logging.debug(here(self))
        self.sem.acquire()

        self.md_buf.close()
        unlink_shared_memory(f'{self.shm_name}-meta')

        if self.shm_buf:
            self.shm_buf.close()
            unlink_shared_memory(self.shm_name)

        self.sem.release()
        self.sem.close()
        logging.info('Writer terminated')

