"""
Wii remote interface that runs in a background thread.
"""

import cwiid
import threading
import time

wiimote = None
buttonsPressed = 0
prevButtonsPressed = 0
server = None
rpt_mode = 0
accelXYZ = (0, 0, 0)

def connect(confirm=False):
    global wiimote
    global server
    global rpt_mode

    print('Press 1 + 2 on your Wii Remote now... ', end='', flush=True)

    time.sleep(1)

    for i in range(3):
        try:
            if i > 0:
                print('Trying again... ', end='', flush=True)
            wiimote = cwiid.Wiimote()
            break
        except RuntimeError:
            pass

    if wiimote is None:
        print('Failed to connect!')
        return False

    print('Wiimote connection established!')
    if confirm:
        rumble(1)
        time.sleep(0.2)
        rumble(0)
    rpt_mode = cwiid.RPT_BTN
    wiimote.rpt_mode = rpt_mode
    time.sleep(0.5)
    while wiimote.state['buttons']:
        pass

    server = WiiRemoteThread()
    thread = threading.Thread(target=server.run, daemon=True)
    thread.start()

    return True

def disconnect():
    server.stop()

def led(l):
    if wiimote:
        wiimote.led = l

def rumble(r):
    if wiimote:
        wiimote.rumble = r

def enableAccel(state):
    if wiimote:
        global rpt_mode
        if state:
            rpt_mode |= cwiid.RPT_ACC
        else:
            rpt_mode &= ~cwiid.RPT_ACC
        wiimote.rpt_mode = rpt_mode

def accel():
    return accelXYZ

def buttons():
    return buttonsPressed

def btnStates():
    global prevButtonsPressed
    btns = buttonsPressed
    pressed = ~prevButtonsPressed & btns
    released = prevButtonsPressed & ~btns
    prevButtonsPressed = btns
    return (btns, pressed, released)

class WiiRemoteThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.exit = False

    def stop(self):
        self.exit = True

    def run(self):
        global wiimote
        global buttonsPressed
        global accelXYZ
        debounce = 0

        while True:
            if debounce == 0:
                if buttonsPressed != wiimote.state['buttons']:
                    debounce = time.monotonic()
            elif time.monotonic() - debounce > 0.001: # 10ms
                buttonsPressed = wiimote.state['buttons']

            if rpt_mode & cwiid.RPT_ACC:
                accelXYZ = wiimote.state['acc']

            time.sleep(0.05)

            if self.exit:
                wiimote = None
                break
