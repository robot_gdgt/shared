"""
Status LED controller
"""

import time
from gpiozero import LED
# from var_dump import var_dump as vd

class StatusLED:

    def __init__(self, pin, on = False):
        self.led = LED(pin)
        if on: self.on()
        else: self.off()

    def on(self):
        self.led.on()
        self.next = None
        # return self

    def off(self):
        self.led.off()
        self.next = None
        # return self

    def blink_once(self, blink_secs = 0.5):
        self.led.on()
        self.next = time.time() + blink_secs
        self.off_secs = None
        # return self

    def heartbeat(self, on_secs = 0.05, off_secs = 1.95):
        self.on_secs = float(on_secs)
        self.off_secs = float(off_secs)
        if self.led.is_lit:
            self.led.off()
            self.next = time.time() + off_secs
        else:
            self.led.on()
            self.next = time.time() + on_secs
        # return self

    def tickle(self):
        if self.next is None or time.time() < self.next: return # return self
        if self.led.is_lit:
            self.led.off()
            self.next = time.time() + self.off_secs if self.off_secs else None
        else:
            self.led.on()
            self.next = time.time() + self.on_secs
        # return self
