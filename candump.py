import time
import struct

from can_ids import *
import can_proxy_thread as can

SOURCES = [
    'Logger', # 0
    'TTS', # 1
    'CANduino sensor left', # 2
    'CANduino sensor right', # 3
    'CANduino sensor top', # 4
    'CANduino motor', # 5
]

TYPES = [
    'bool', # 0
    'char', # 1
    'uint8', # 2
    'int8', # 3
    'uint16', # 4
    'int16', # 5
    'uint32', # 6
    'int32', # 7
    'float', # 8
    'double', # 9
    '?',
    '?',
    '?',
    '?',
    '?',
    'str', # 15
]

##### SETUP #####

can.add_filter('candump', CAN_LOGGING, 0x700)

##### LOOP #####

print('READY')

lastCANrx = time.monotonic()

while True:

    CANrx = can.get('candump')
    if CANrx is not None:
        if lastCANrx + 1.0 < time.monotonic():
            print('-----')

        # print(f'CAN Rx id: {format(CANrx["id"], "#X")} msg: {CANrx}')
        isource = (CANrx["id"] & 0xF0) >> 4
        # print(f'source: {source}')
        source = SOURCES[isource] if isource < len(SOURCES) else f'Unknown ({isource})'

        itype = CANrx["id"] & 0x0F
        # print(f'type: {type}')
        type = TYPES[itype] if itype < len(TYPES) else '?'

        if type == 'str':
            str = ''
            for c in CANrx["data"]:
                str = str + chr(c)
            print(f'{source} *** {str} ***')
        elif type == '?':
            print(f'{source} - Unknown type: {itype}')
        else:
            if type == 'bool':
                v = 'true' if CANrx["data"][0] else 'false'
                dl = 1
            elif type == 'char':
                v = CANrx["data"][0]
                dl = 1
            elif type == 'uint8':
                v = int.from_bytes(CANrx["data"][0:1], 'big')
                dl = 1
            elif type == 'int8':
                v = int.from_bytes(CANrx["data"][0:1], 'big', signed=True)
                dl = 1
            elif type == 'uint16':
                v = int.from_bytes(CANrx["data"][0:2], 'big')
                dl = 2
            elif type == 'int16':
                v = int.from_bytes(CANrx["data"][0:2], 'big', signed=True)
                dl = 2
            elif type == 'uint32':
                v = int.from_bytes(CANrx["data"][0:4], 'big')
                dl = 4
            elif type == 'int32':
                v = int.from_bytes(CANrx["data"][0:4], 'big', signed=True)
                dl = 4
            elif type == 'float':
                v = round(struct.unpack('<f', bytes(CANrx["data"][0:4]))[0], 3)
                dl = 4
            else:
                dl = 0
            str = ''
            if dl > 0:
                for c in CANrx["data"][dl:]:
                    str = str + chr(c)
                str = ' - ' + str

            print(f'{source}{str} ({type}): {v}')

        lastCANrx = time.monotonic()
