import time
import subprocess
# from var_dump import var_dump as vd

class StatusLED:

    def __init__(self, pin, on = False):
        self.pin = pin
        subprocess.run(f"gpio mode {self.pin} out", shell=True)
        self.state = 0
        if on: self.on()
        else: self.off()

    def on(self):
        subprocess.run(f"gpio write {self.pin} 1", shell=True)
        self.state = 1
        self.next = None
        # return self

    def off(self):
        subprocess.run(f"gpio write {self.pin} 0", shell=True)
        self.state = 0
        self.next = None
        # return self

    def blink_once(self, blink_secs = 0.5):
        self.on()
        self.next = time.time() + blink_secs
        # return self

    def heartbeat(self, on_secs = 0.05, off_secs = 1.95):
        self.on_secs = float(on_secs)
        self.off_secs = float(off_secs)
        if self.state:
            self.off()
            self.next = time.time() + off_secs
        else:
            self.on()
            self.next = time.time() + on_secs
        # return self

    def tickle(self):
        if self.next is None or time.time() < self.next: return # return self
        if self.state:
            self.off()
            self.next = time.time() + self.off_secs
        else:
            self.on()
            self.next = time.time() + self.on_secs
        # return self
