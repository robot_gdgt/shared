"""
CAN interface module with process queues

Subscribers can:
Add or delete filter/mask (CANfilter queue)
	The same filter/mask combo can be request by more than one subsciber
Send CAN packets (CANtx queue)
	automatic loopback to other subscribers

When an incoming CAN packet is received:
Placed in subscriber queue based on filter/mask (<FILE> queue)
"""

import subprocess
from os import path
# import logging
# import logger
import can
from queue import Queue
# from gdgt_platform import *
from can_ids import CAN_BUS_SPEED
from var_dump import var_dump as vd

class LOGGER:
    def debug(self, s):
        vd(s)

    def info(self, s):
        vd(s)

FILE = path.splitext(path.basename(__file__))[0]

# logger.init(FILE, logging.INFO)
logging = LOGGER()

# Initialize

subprocess.run("sudo ip link set can0 down", shell=True)
subprocess.run(f"sudo ip link set can0 up type can bitrate {CAN_BUS_SPEED}", shell=True)
bus = can.interface.Bus(bustype='socketcan_native', channel='can0')
can_filters = []
bus.set_filters(can_filters)
rx_queues = {}

# Add a CAN filter

def add_filter(req_by, id, mask):
    # First make sure the requestor haas a RX queue
    if req_by not in rx_queues:
        rx_queues[req_by] = Queue(100)

    # CAN filters can be set by more than one requestor. When adding a filter it is added
    # only when there are no other requestors for that id/mask combination. If there are
    # the req_by of the existing id/mask combination is updated with the new req_by.
    found = False
    for i, filter in enumerate(can_filters):
        if id == filter['can_id'] and mask == filter['can_mask']:
            filter['req_by'].add(req_by)
            found = True
            break
    if not found:
        filter = {"can_id": id, "can_mask": mask, "extended": False, "req_by": {req_by,}}
        can_filters.append(filter)
        bus.set_filters(can_filters)
    logging.debug(can_filters)

# Delete a CAN filter

def del_filter(req_by, id, mask):
    # CAN filters can be set by more than one requestor. When deleting a filter it is removed
    # only when there are no more requestors for that id/mask combination.
    for i, filter in enumerate(can_filters):
        if id == 'all' or (id == filter['can_id'] and mask == filter['can_mask']):
            filter['req_by'].discard(req_by)
            if len(filter['req_by']) == 0:
                del can_filters[i]
                logging.debug(can_filters)
                bus.set_filters(can_filters)
            break

# Get CAN messages and add them to the RX queues

def check():
    new_msg = bus.recv(0.0)
    while new_msg is not None:
        logging.debug(new_msg)
        msg = {
            'id': new_msg.arbitration_id,
            'is_remote_frame': new_msg.is_remote_frame,
            'dlc': new_msg.dlc,
            'data': [x for x in new_msg.data]
        }
        logging.info(f'CAN recd: {msg}')
        for filter in can_filters:
            if msg['id'] & filter['can_mask'] == filter['can_id']:
                for req_by in filter['req_by']:
                    logging.info(f'Send: {msg} to: {req_by}')
                    rx_queues[req_by].put_nowait(msg)
        new_msg = bus.recv(0.0)

# Get a CAN message from a RX queue

def get(req_by):
    if req_by not in rx_queues:
        return None
    if rx_queues[req_by].empty():
        return None
    return rx_queues[req_by].get()

# Send CAN message immediately

def send(id, irf=False, data=[]):
    msg = can.Message(
        arbitration_id = id,
        is_extended_id = False,
        is_remote_frame = irf,
        dlc = len(data),
        data = data)
    # bus.send(msg, timeout=0.2)
    bus.send(msg)
    logging.info(f'Tx: {msg}')

    msg = {
        'id': id,
        'is_remote_frame': irf,
        'dlc': len(data),
        'data': data
    }
    for filter in can_filters:
        if id & filter['can_mask'] == filter['can_id']:
            for req_by in filter['req_by']:
                logging.info(f'Send: {msg} to: {req_by}')
                rx_queues[req_by].put_nowait(msg)
