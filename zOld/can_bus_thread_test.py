from time import sleep, time

from status_led import StatusLED
from gdgt_platform import *
import can_bus_thread
from can_ids import *

hbLED = StatusLED(LED_PIN, True)

def recvCallback(msg):
    print(f'recd: {msg}')

can_bus_thread.msgRecdCallback = recvCallback

can_filters = [
    {"can_id": CAN_CMD_HALT, "can_mask": 0x7FF, "extended": False},
    {"can_id": CAN_TS_VIS_OBST, "can_mask": 0x7FF, "extended": False},
    {"can_id": CAN_CMD_SUPER, "can_mask": 0x7FF, "extended": False},
    {"can_id": 0x000, "can_mask": 0x7F8, "extended": False}, # Want all alert packets
    {"can_id": 0x500, "can_mask": 0x7F8, "extended": False}, # Want all status packets
]
can_bus_thread.set_filters(can_filters)

hbLED.heartbeat()

t = time()
while True:
    if time() - t > 5:
        can_bus_thread.send({
            'can_id': CAN_STATUS_BUMPER,
            'dlc': 3,
            'data': [0x00, 0x01, 0x02]
        })
        t = time()

    hbLED.tickle()

    sleep(0.01)
