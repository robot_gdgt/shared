"""
CAN interface that runs in a background thread.
Outgoing CAN messages are put in a queue and sent by the CanBusThread.run loop.
Incoming CAN messages are sent to the supplied callback function immediately.
"""

import can
import json
from queue import Queue
import subprocess
import threading
import time

from can_ids import CAN_BUS_SPEED

subprocess.run("sudo ip link set can0 down", shell=True)
subprocess.run(f"sudo ip link set can0 up type can bitrate {CAN_BUS_SPEED}", shell=True)
bus = can.interface.Bus(bustype='socketcan_native', channel='can0')

msgRecdCallback = None
q = Queue(maxsize=100)

def set_filters(can_filters):
    bus.set_filters(can_filters)

def send(new_msg, timeout = 0.1):
    assert ('can_id' in new_msg), "CAN ID missing"
    q.put((new_msg, timeout))

def down():
    subprocess.run("sudo ip link set can0 down", shell=True)

class CanBusThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while True:
            msg = bus.recv(0.0)
            if msg is not None:
                if msgRecdCallback is not None:
                    msgRecdCallback({
                        'can_id': msg.arbitration_id,
                        'irf': msg.is_remote_frame,
                        'dlc': msg.dlc,
                        'data': [x for x in msg.data]
                    })
            if not q.empty():
                (new_msg, timeout) = q.get()
                irf = new_msg['irf'] if 'irf' in new_msg else False
                dlc = new_msg['dlc'] if 'dlc' in new_msg else (len(new_msg['data']) if 'data' in new_msg else 0)
                data = new_msg['data'] if 'data' in new_msg else []
                msg = can.Message(
                    arbitration_id=new_msg['can_id'],
                    is_extended_id=False,
                    is_remote_frame=irf,
                    dlc=dlc,
                    data=data)
                bus.send(msg, timeout=timeout)
                print(f'CAN Rx: {msg}')
                q.task_done()

            time.sleep(0.001)

server = CanBusThread()
threading.Thread(target=server.run, daemon=True).start()
