import sys

def here(obj = None):
    if obj:
        return f'{obj.__class__.__name__}.{sys._getframe(1).f_code.co_name}'
    else:
        return sys._getframe(1).f_code.co_name
