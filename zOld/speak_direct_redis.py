import json
from can_ids import *

def speak_direct(red, str):
    bytes = []
    n = 0
    str = str + chr(10)
    for byte in str:
        bytes.append(ord(byte))
        if len(bytes) == 8:
            red.rpush('CANtx', json.dumps({
                'can_id': CAN_SPEAK_DIRECT,
                'dlc': 8,
                'data': bytes
            }))
            bytes = []
    if len(bytes) > 0:
        red.rpush('CANtx', json.dumps({
            'can_id': CAN_SPEAK_DIRECT,
            'dlc': len(bytes),
            'data': bytes
        }))
