import can_bus
from can_ids import *

def speak_direct(str):
    bytes = []
    str = str + chr(10)
    for byte in str:
        bytes.append(ord(byte))
        if len(bytes) == 8:
            can_bus.send({
                'can_id': CAN_SPEAK_DIRECT,
                'dlc': 8,
                'data': bytes
            })
            bytes = []
    if len(bytes) > 0:
        can_bus.send({
            'can_id': CAN_SPEAK_DIRECT,
            'dlc': len(bytes),
            'data': bytes
        })
