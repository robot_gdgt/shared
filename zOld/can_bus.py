import can
import subprocess
from can_ids import *

print(f'{__file__}.__init__()')
subprocess.run("sudo ip link set can0 down", shell=True)
subprocess.run(f"sudo ip link set can0 up type can bitrate {CAN_BUS_SPEED}", shell=True)
bus = can.interface.Bus(bustype='socketcan_native', channel='can0')
print(f'__init__ bus: {bus}')

def init():
    global bus
    print(f'{__file__}.__init__()')
    subprocess.run("sudo ip link set can0 down", shell=True)
    subprocess.run(f"sudo ip link set can0 up type can bitrate {CAN_BUS_SPEED}", shell=True)
    bus = can.interface.Bus(bustype='socketcan_native', channel='can0')
    print(f'init bus: {bus}')

def set_filters(can_filters):
    if bus == None:
        print('set_filters bus == None')
        return
    bus.set_filters(can_filters)

def recv(timeout = 0.0):
    if bus == None:
        print('recv bus == None')
        return
    msg = bus.recv(timeout)
    if msg is None:
        return None
    return {
        'can_id': msg.arbitration_id,
        'is_remote_frame': msg.is_remote_frame,
        'dlc': msg.dlc,
        'data': [x for x in msg.data]
    }

def send(new_msg, timeout = 1.0):
    if bus == None:
        print('send bus == None')
        return
    print(f'send {new_msg}')
    assert ('can_id' in new_msg), "CAN ID missing"
    irf = new_msg['irf'] if 'irf' in new_msg else False
    dlc = new_msg['dlc'] if 'dlc' in new_msg else (len(new_msg['data']) if 'data' in new_msg else 0)
    data = new_msg['data'] if 'data' in new_msg else []
    msg = can.Message(
        arbitration_id=new_msg['can_id'],
        is_extended_id=False,
        is_remote_frame=irf,
        dlc=dlc,
        data=data)
    bus.send(msg, timeout=timeout)
    print(f'sent {msg}')

def down():
    subprocess.run("sudo ip link set can0 down", shell=True)
