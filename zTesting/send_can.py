import can
import subprocess

from can_ids import *
from can_ids_dict import getCan

print("ip link set can0 down")
subprocess.run("sudo ip link set can0 down", shell=True)
print("ip link set can0 up")
subprocess.run("sudo ip link set can0 up type can bitrate {}".format(CAN_BUS_SPEED), shell=True)
print("can.interface.Bus")
bus = can.interface.Bus(bustype='socketcan_native', channel='can0')

while(True):
    userInput = str(input('Enter data: '))
    if userInput.lower() == 'q':
        break

    if userInput.lower() != 'r':
        id = None
        bytes = []
        for byte in userInput.split():
            if id == None:
                id = getCan(byte)
                if id is None:
                    print(f'{byte} not found')
                    break
            elif len(byte) == 1:
                bytes.append(ord(byte))
            else:
                byte = int(byte, 0)
                if byte >= 0 and byte <= 255:
                    bytes.append(byte)

    if id is None:
        continue

    print(f'id: {id} data: {bytes}')
    try:
        msg = can.Message(arbitration_id=id, data=bytes, extended_id=False)
        bus.send(msg)
        print("Message sent on {}".format(bus.channel_info))
    except can.CanError as e:
        print(e)
