import time

from status_led import StatusLED
from gdgt_platform import *

print(f'LED_PIN = {LED_PIN}')
hbLED = StatusLED(LED_PIN, True)

time.sleep(2)

hbLED.heartbeat()

while True:
    hbLED.tickle()

    time.sleep(0.01)
