from speak_direct_can_thread import speak_direct
# from var_dump import var_dump as vd

speak_direct('Hello.')

while (True):
    userInput = str(input('Enter string: '))
    userInput = userInput.strip()
    if userInput.lower() == 'q':
        break

    speak_direct(userInput)
