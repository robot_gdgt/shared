
import cwiid
import time

from RollingMedian import RollingMedian
import wii_remote_thread as wii

wii.connect(confirm=True)

prev = 0
i = 0
l = 0
accelState = False
rmX = RollingMedian(9)
rmY = RollingMedian(9)
rmZ = RollingMedian(9)
prevX = None
prevY = None
prevZ = None

while True:
#     btns = wii.buttons()
#     if btns & cwiid.BTN_1:
#         wii.led(1)
#     print(btns)

    (btns, pressed, released) = wii.btnStates()
    if btns != prev:
        print(f'btns: {btns}')
        prev = btns
    if pressed:
        print(f'pressed: {pressed}')
    if released:
        print(f'released: {released}')

    if accelState:
#         print(f'accel: {wii.accel()}')
        x,y,z = wii.accel()
        if accelState == 1:
            rmX.add(x)
            temp = rmX.get()
            if temp != prevX:
                print(f'X: {temp}')
                prevX = temp
        elif accelState == 2:
            rmY.add(y)
            temp = rmY.get()
            if temp != prevY:
                print(f'Y: {temp}')
                prevY = temp
        else:
            rmZ.add(z)
            temp = rmZ.get()
            if temp != prevZ:
                print(f'Z: {temp}')
                prevZ = temp

    i += 1
    if i == 10000:
        l = (l + 1) % 4
        wii.led(2 ** l)
        i = 0

    if pressed == cwiid.BTN_HOME:
        wii.disconnect()
        print('exiting')
        time.sleep(5)
        exit()

    if released == cwiid.BTN_1:
        accelState = (accelState + 1) % 4
        wii.enableAccel(accelState > 0)
