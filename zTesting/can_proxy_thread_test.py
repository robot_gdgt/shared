import time

import can_proxy_thread as can
from can_ids import *
from var_dump import var_dump as vd

can.add_filter('test', 0x000, 0x700)
can.add_filter('test', 0x100, 0x700)
can.add_filter('test', 0x500, 0x700)

can.del_filter('test', 0x100, 0x700)

can.send('test', 0x011)

msg = can.get('test')
print(msg)

can.send(0x011, False, [100, 200, 0])

while True:
    msg = can.get('test')
    if msg is not None:
        print(msg)

    time.sleep(0.1)
