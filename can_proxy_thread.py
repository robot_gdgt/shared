"""
CAN interface that runs in a background thread.

Subscribers can:
Add or delete filter/mask (CANfilter queue)
	The same filter/mask combo can be request by more than one subsciber
Send CAN packets (CANtx queue)
	automatic loopback to other subscribers

When an incoming CAN packet is received:
Placed in subscriber queue based on filter/mask (<FILE> queue)
"""

import can
import datetime
import logging
from os import path
from queue import SimpleQueue
import shutil
import subprocess
import threading
import time

from can_ids import CAN_BUS_SPEED
# import logger
# from var_dump import var_dump as vd

# FILE = path.splitext(path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

if path.exists('/home/pi/logs/can.log'):
    shutil.copyfile('/home/pi/logs/can.log', '/home/pi/logs/can0.log')
now = datetime.datetime.now()
with open('/home/pi/logs/can.log', 'w') as fp:
    fp.write(now.strftime("%Y-%m-%d %H:%M:%S\n"))
    fp.close()
logging.basicConfig(filename='/home/pi/logs/can.log', level=logging.DEBUG)

# Initialize

subprocess.run("sudo ip link set can0 down", shell=True)
subprocess.run(f"sudo ip link set can0 up type can bitrate {CAN_BUS_SPEED}", shell=True)
bus = can.interface.Bus(bustype='socketcan_native', channel='can0')
can_filters = []
bus.set_filters(can_filters)
rx_queues = {}
tx_queue = SimpleQueue()

# Add a CAN filter

def add_filter(req_by, id, mask):
    # First make sure the requestor has a RX queue
    if req_by not in rx_queues:
        rx_queues[req_by] = SimpleQueue()

    # CAN filters can be set by more than one requestor. When adding a filter it is added
    # only when there are no other requestors for that id/mask combination. If there are
    # the req_by of the existing id/mask combination is updated with the new req_by.
    found = False
    for i, filter in enumerate(can_filters):
        if id == filter['can_id'] and mask == filter['can_mask']:
            if req_by not in filter['req_by']:
                filter['req_by'].add(req_by)
            found = True
            break
    if not found:
        filter = {"can_id": id, "can_mask": mask, "extended": False, "req_by": {req_by,}}
        can_filters.append(filter)
        bus.set_filters(can_filters)
    logging.debug(can_filters)

# Delete a CAN filter

def del_filter(req_by, id, mask=0x000):
    # CAN filters can be set by more than one requestor. When deleting a filter it is removed
    # only when there are no more requestors for that id/mask combination.
    for i, filter in enumerate(can_filters):
        if id == filter['can_id'] and mask == filter['can_mask'] and req_by in filter['req_by']:
            filter['req_by'].discard(req_by)
            if len(filter['req_by']) == 0:
                del can_filters[i]
                bus.set_filters(can_filters)
    found = False
    for i, filter in enumerate(can_filters):
        if req_by in filter['req_by']:
            found = True
    if not found and req_by in rx_queues:
        del rx_queues[req_by]
    logging.debug(can_filters)

def del_filters(req_by):
    # CAN filters can be set by more than one requestor. When deleting a filter it is removed
    # only when there are no more requestors for that id/mask combination.
    for i, filter in enumerate(can_filters):
        if req_by in filter['req_by']:
            filter['req_by'].discard(req_by)
            if len(filter['req_by']) == 0:
                del can_filters[i]
                bus.set_filters(can_filters)
    if req_by in rx_queues:
        del rx_queues[req_by]
    logging.debug(can_filters)

# Get a CAN message from a RX queue

def get(req_by):
    if req_by not in rx_queues:
        return None
    if rx_queues[req_by].empty():
        return None
    return rx_queues[req_by].get()

# Get the last CAN message from a RX queue, ignoring any other messages

def getLast(req_by):
    if req_by not in rx_queues:
        return None
    if rx_queues[req_by].empty():
        return None
    lastMsg = None
    while True:
        nextMsg = rx_queues[req_by].get()
        if nextMsg is None:
            return lastMsg
        lastMsg = nextMsg

# Put a CAN message into the TX queue

def send(sender, id, rr=False, data=[]):
    msg = {
        'sender': sender,
        'id': id,
        'rr': rr,
        'dl': 0 if rr else len(data),
        'data': [] if rr else data
    }
    logging.info(f'tx send by {sender}: {hex(id)} rr:{rr} daat:{data}')
    tx_queue.put_nowait(msg)

def down():
    subprocess.run("sudo ip link set can0 down", shell=True)

class CanBusThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while True:
            while True:
                can_msg = bus.recv(0.0)
                if can_msg is None:
                    break
                logging.info(f'RX: {can_msg}')
                msg = {
                    'id': can_msg.arbitration_id,
                    'rr': can_msg.is_remote_frame,
                    'dl': can_msg.dlc,
                    'data': [x for x in can_msg.data]
                }

                for filter in can_filters:
                    if msg['id'] & filter['can_mask'] == filter['can_id']:
                        for req_by in filter['req_by']:
                            logging.info(f'tx {req_by} - id: {format(msg["id"], "#X")}, rr: {msg["rr"]}, dl: {msg["dl"]}, data: {msg["data"]}')
                            # print(f'rx {req_by} - id: {format(msg["id"], "#X")}, rr: {msg["rr"]}, dl: {msg["dl"]}, data: {msg["data"]}')
                            try:
                                rx_queues[req_by].put_nowait(msg)
                            except:
                                logging.debug(f'failed to add CAN msg to rx_queues[{req_by}]')

            if tx_queue.empty() == False:
                msg = tx_queue.get_nowait()
                can_msg = can.Message(
                    arbitration_id = msg['id'],
                    is_extended_id = False,
                    is_remote_frame = msg['rr'],
                    dlc = msg['dl'],
                    data = msg['data']
                )
                bus.send(can_msg) # , timeout=0.2)
                logging.info(f'TX: {can_msg}')

                for filter in can_filters:
                    if msg['id'] & filter['can_mask'] == filter['can_id']:
                        for req_by in filter['req_by']:
                            if req_by == msg['sender']:
                                continue
                            logging.info(f'tx {req_by} - id: {format(msg["id"], "#X")}, rr: {msg["rr"]}, dl: {msg["dl"]}, data: {msg["data"]}')
                            # print(f'tx {req_by} - id: {format(msg["id"], "#X")}, rr: {msg["rr"]}, dl: {msg["dl"]}, data: {msg["data"]}')
                            try:
                                rx_queues[req_by].put_nowait(msg)
                            except:
                                logging.debug(f'failed to add CAN msg to rx_queues[{req_by}]')

            time.sleep(0.01)

server = CanBusThread()
threading.Thread(target=server.run, daemon=True).start()
