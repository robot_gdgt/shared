from can_ids import *
import can_proxy_thread as can

##### SETUP #####

can.del_filter('main', 'all', 'all')
can.add_filter('main', 0, 0)

##### LOOP #####

print('READY')

while True:

    CANrx = can.get('main')
    if CANrx is not None:
        print(f'CAN Rx id: {format(CANrx["id"], "#X")} msg: {CANrx}')
