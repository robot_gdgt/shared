import redis
from gdgt_platform import *

def init(unix_socket_path=REDIS_SOCKET, decode_responses=True):
    try:
        red = redis.Redis(unix_socket_path=unix_socket_path, decode_responses=decode_responses)
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        raise Exception('Is the redis server running?')

    return red